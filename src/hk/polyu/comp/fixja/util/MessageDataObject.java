package hk.polyu.comp.fixja.util;

import java.util.Map;
import java.util.Set;

import fixjaview.views.SampleView;
import hk.polyu.comp.fixja.fixaction.FixAction;
import hk.polyu.comp.fixja.monitor.LineLocation;

public class MessageDataObject {
	
	int generatedFixAction = 0;
	static Map<LineLocation, Set<FixAction>> gFixActions;
	int stage = 0 ;
	int validatingFixNo = 0; 
	
	//Publisher start
	public static void publisher() {
				
		dispatcher();
	}
	
	public static void publishFixActionEntry (Map<LineLocation, Set<FixAction>> fixActions) {
		
		gFixActions = fixActions;	
		
		
		notifyFixActions(gFixActions);
	}
	
	
	//Publisher End

	private static void dispatcher() {
		
		
		notifytest() ;
	}
	

	
	//Call receivers
	private static void notifytest() {
		
		SampleView.receiverTest();			
	}

	private static void notifyFixActions(Map<LineLocation, Set<FixAction>> fixActions) {
        int size = 0;
        for (Map.Entry<LineLocation, Set<FixAction>> fixActionEntry : fixActions.entrySet()) {
            size += fixActionEntry.getValue().size();
/*            LoggingService.putFixActionMap(fixActionEntry);
 * 				
*/
           // MessageDataObject.publisher();
        }
		SampleView.receiverFixActions(fixActions,size)	;		
	}
	

}
