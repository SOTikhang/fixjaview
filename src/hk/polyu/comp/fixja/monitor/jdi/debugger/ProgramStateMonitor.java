package hk.polyu.comp.fixja.monitor.jdi.debugger;

import com.sun.jdi.*;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.tools.example.debug.expr.ExpressionParser;
import hk.polyu.comp.fixja.fixer.config.FailureHandling;
import hk.polyu.comp.fixja.fixer.config.FixerOutput;
import hk.polyu.comp.fixja.fixer.log.LogLevel;
import hk.polyu.comp.fixja.java.JavaProject;
import hk.polyu.comp.fixja.monitor.ExpressionToMonitor;
import hk.polyu.comp.fixja.monitor.LineLocation;
import hk.polyu.comp.fixja.monitor.state.DebuggerEvaluationResult;
import hk.polyu.comp.fixja.monitor.state.ProgramState;
import org.eclipse.jdt.core.dom.*;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static hk.polyu.comp.fixja.util.CommonUtils.isSubExp;

public class ProgramStateMonitor extends AbstractDebuggerLauncher {

    private Map<Integer, LineLocation> allLocations;

    private List<BreakpointRequest> breakpointRequestsForMonitoring;
    private com.sun.jdi.Location mtfEntryLocation;
    private com.sun.jdi.Location mtfExitLocation;
    private int nbrStackFramesAtMethodEntry;

    public ProgramStateMonitor(JavaProject project, LogLevel logLevel, long timeoutPerTest,
                               FailureHandling failureHandling, boolean shouldExitUponKilledTest, Set<LineLocation> allLocations) {
        super(project, logLevel, timeoutPerTest, failureHandling, shouldExitUponKilledTest);

        this.allLocations = allLocations.stream().collect(Collectors.toMap(LineLocation::getLineNo, Function.identity()));
    }

    // ============================================== Override

    @Override
    protected void processTestStart(BreakpointEvent breakpointEvent) {
        super.processTestStart(breakpointEvent);

        nbrStackFramesAtMethodEntry = -1;

        if (breakpointRequestsForMonitoring != null) breakpointRequestsForMonitoring.forEach(x -> x.enable());
    }

    @Override
    protected void processTestEnd(BreakpointEvent breakpointEvent) {
        super.processTestEnd(breakpointEvent);

        if (breakpointRequestsForMonitoring != null) breakpointRequestsForMonitoring.forEach(x -> x.disable());
    }

    @Override
    protected String testsToRunInString() throws Exception {
        return getProject().commandLineArgumentForTestsToRun();
    }

    @Override
    protected Path getLogPath() {
        return FixerOutput.getMonitoredTestResultsLogFilePath();
    }

    @Override
    protected void debuggerFinished() {
    }

    @Override
    protected void registerBreakpointForMonitoring(ReferenceType referenceType, boolean shouldEnable) throws AbsentInformationException {
        Method methodToMonitor = getMethodToMonitorFromType(referenceType);
        breakpointRequestsForMonitoring = addBreakPointToAllLocationsInMethod(methodToMonitor, shouldEnable);
        setMethodEntryAndExitLocation(methodToMonitor);
    }

    @Override
    protected void processMonitorLocation(BreakpointEvent breakpointEvent) throws AbsentInformationException {
        int lineNo = breakpointEvent.location().lineNumber();
        ExpressionParser.GetFrame getFrame = getFrameGetter(breakpointEvent.thread(), 0, null);

        if (lineNo == mtfEntryLocation.lineNumber() && nbrStackFramesAtMethodEntry == -1) {
            nbrStackFramesAtMethodEntry = safeGetNbrStackFrames(breakpointEvent);
        } else if (lineNo == mtfExitLocation.lineNumber() && nbrStackFramesAtMethodEntry == safeGetNbrStackFrames(breakpointEvent)) {
            nbrStackFramesAtMethodEntry = -1;
            Set<ExpressionToMonitor> expressionToMonitorSet = getProject().getMethodToMonitor().getMethodDeclarationInfoCenter()
                    .getExpressionsToMonitorAtMethodExit();

            ProgramState exitState = new ProgramState(getProject().getMethodToMonitor().getMethodDeclarationInfoCenter().getExitLocation());
            Set<ExpressionToMonitor> invalidExpressions = new HashSet<>();
            for (ExpressionToMonitor etm : expressionToMonitorSet) {
                DebuggerEvaluationResult debuggerEvaluationResult = evaluate(getVirtualMachine(), getFrame, etm);

                if (debuggerEvaluationResult.hasSyntaxError())
                    invalidExpressions.add(etm);

                if (!debuggerEvaluationResult.hasSemanticError() && !debuggerEvaluationResult.hasSyntaxError())
                    exitState.extend(etm, debuggerEvaluationResult);
            }
            // Remove expressions that are invalid at this location
            expressionToMonitorSet.removeAll(invalidExpressions);

            getCurrentTestResult().addExitState(exitState.getExtendedState(getProject().getMethodToMonitor()
                    .getMethodDeclarationInfoCenter().getStateSnapshotExpressionsAtMethodExit()));
        } else {
            if (allLocations.containsKey(lineNo)) {
                LineLocation location = allLocations.get(lineNo);
                Set<ExpressionToMonitor> expressionToMonitorSet = getProject().getMethodToMonitor().getMethodDeclarationInfoCenter()
                        .getLocationExpressionMap().get(location);

                ProgramState state = monitoring(getFrame, expressionToMonitorSet, location);
                getCurrentTestResult().getObservedStates().add(state);
            }
        }
    }

    // ============================================== Implementation details

    private void setMethodEntryAndExitLocation(Method method) {
        try {
            List<com.sun.jdi.Location> locations = method.allLineLocations();
            for (com.sun.jdi.Location location : locations) {
                // First location as the entry location
                if (mtfEntryLocation == null || mtfEntryLocation.lineNumber() > location.lineNumber())
                    mtfEntryLocation = location;

                // Last location as the exit location
                if (mtfExitLocation == null || mtfExitLocation.lineNumber() < location.lineNumber())
                    mtfExitLocation = location;
            }
        } catch (AbsentInformationException e) {
            e.printStackTrace();
        }
    }

    private ProgramState monitoring(ExpressionParser.GetFrame getFrame, Set<ExpressionToMonitor> expressionToMonitorSet, LineLocation location) {
        ProgramState state = new ProgramState(location);

        Set<ExpressionToMonitor> invalidExpressions = new HashSet<>();
        for (ExpressionToMonitor etm : expressionToMonitorSet) {
            if (isSpecialCase(location, etm)) continue;

            DebuggerEvaluationResult debuggerEvaluationResult = evaluate(getVirtualMachine(), getFrame, etm);

            if (debuggerEvaluationResult.hasSyntaxError())
                invalidExpressions.add(etm);

            if (!debuggerEvaluationResult.hasSemanticError() && !debuggerEvaluationResult.hasSyntaxError())
                state.extend(etm, debuggerEvaluationResult);
        }
        expressionToMonitorSet.removeAll(invalidExpressions);

        return state;
    }

    // fixme: is this necessary?
    public boolean isSpecialCase(LineLocation location, ExpressionToMonitor exp) {
        Map<LineLocation, Statement> locationStatementMap = getProject().getMethodToMonitor().getMethodDeclarationInfoCenter().getRelevantLocationStatementMap();
        if (locationStatementMap.containsKey(location)) {
            Statement oldStmt = locationStatementMap.get(location);
            if (oldStmt instanceof ForStatement) {
                ForStatement forStmt = (ForStatement) oldStmt;
                for (Object init : forStmt.initializers()) {
                    if (init instanceof VariableDeclarationExpression) {
                        VariableDeclarationExpression initExp = (VariableDeclarationExpression) init;
                        for (Object o : initExp.fragments()) {
                            VariableDeclarationFragment oexp = (VariableDeclarationFragment) o;
                            if (isSubExp(exp, oexp.getName()))
                                return true;
                        }
                    }
                }
            } else if (oldStmt instanceof EnhancedForStatement) {
                EnhancedForStatement forStmt = (EnhancedForStatement) oldStmt;
                SingleVariableDeclaration initExp = (SingleVariableDeclaration) forStmt.getParameter();
                if (isSubExp(exp, initExp.getName()))
                    return true;
            }
            return false;
        }
        return false;
    }
}
