package hk.polyu.comp.fixja.monitor.jdi.debugger;

import hk.polyu.comp.fixja.monitor.snapshot.StateSnapshotExpression;
import hk.polyu.comp.fixja.monitor.state.ProgramState;
import hk.polyu.comp.fixja.tester.TestRequest;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Max PEI.
 */
public class TestExecutionResult {
    private String testClass;
    private String testMethod;
    private long runTime;
    private boolean wasSuccessful;
    private boolean wasTestKilled;

    private List<ProgramState> observedStates;
    private List<ProgramState> derivedStates;

    private List<ProgramState> exitStates;

    public TestExecutionResult(String testClass, String testMethod) {
        this.testClass = testClass;
        this.testMethod = testMethod;
        this.runTime = 0;
        this.wasSuccessful = false;
        this.wasTestKilled = false;
    }

    public boolean isForRequest(TestRequest request){
        return getTestClass().equals(request.getTestClass()) && getTestMethod().equals(request.getTestMethod());
    }

    public String getTestClass() {
        return testClass;
    }

    public String getTestMethod() {
        return testMethod;
    }

    public String getTestClassAndMethod(){
        return testClass + "." + testMethod;
    }

    public double computeSimilarity(TestExecutionResult other){
        if(!getTestClassAndMethod().equals(other.getTestClassAndMethod()))
            throw new IllegalStateException();

        Set<Integer> thisObservedLocations = getObservedStates().stream().mapToInt(x -> x.getLocation().getLineNo()).boxed().collect(Collectors.toSet());
        Set<Integer> otherObservedLocations = other.getObservedStates().stream().mapToInt(x -> x.getLocation().getLineNo()).boxed().collect(Collectors.toSet());
        double locationSimilarity = ((double)thisObservedLocations.size() + 1) / (thisObservedLocations.size() + otherObservedLocations.size() + 1);

        int minSize = Math.min(getExitStates().size(), other.getExitStates().size());
        double stateSimilarity;
        if(minSize == 0) {
            // fixme: find out when/why this condition may be true
            // throw new IllegalStateException();
            stateSimilarity = MINIMAL_SIMILARITY;
        }
        else {
            List<Double> stateSimilarities = new LinkedList<>();
            for (int i = 0; i < minSize; i++) {
                stateSimilarities.add(getExitStates().get(i).computeSimilarity(other.getExitStates().get(i)));
            }
            stateSimilarity = stateSimilarities.stream().mapToDouble(x -> x).summaryStatistics().getAverage();
        }
        return 2.0 / ( 1.0 / locationSimilarity + 1.0 / stateSimilarity);
    }

    public static final double MINIMAL_SIMILARITY = 0.1;

    public long getRunTime() {
        return runTime;
    }

    public List<ProgramState> getObservedStates() {
        if(observedStates == null)
            observedStates = new LinkedList<>();

        return observedStates;
    }

    public void deriveStatesUsingStateSnapshotExpressions(Set<StateSnapshotExpression> stateSnapshotExpressions){
        derivedStates = new LinkedList<>();
        getObservedStates().forEach(x -> getDerivedStates().add(x.getExtendedState(stateSnapshotExpressions)));
    }

    public List<ProgramState> getDerivedStates(){
        if(derivedStates == null)
            throw new IllegalStateException();

        return derivedStates;
    }

    public boolean wasSuccessful() {
        return !wasTestKilled && wasSuccessful;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    public void setWasSuccessful(boolean wasSuccessful) {
        this.wasSuccessful = wasSuccessful;
    }

    public boolean wasTestKilled() {
        return wasTestKilled;
    }

    public void setTestKilled(boolean wasTestKilled) {
        this.wasTestKilled = wasTestKilled;
    }

    public List<ProgramState> getExitStates() {
        if(exitStates == null)
            exitStates = new LinkedList<>();

        return exitStates;
    }

    public void addExitState(ProgramState exitState) {
        this.getExitStates().add(exitState);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestExecutionResult that = (TestExecutionResult) o;

        if (getRunTime() != that.getRunTime()) return false;
        if (wasSuccessful != that.wasSuccessful) return false;
        if (!getTestClass().equals(that.getTestClass())) return false;
        return getTestMethod().equals(that.getTestMethod());
    }

    @Override
    public int hashCode() {
        int result = getTestClass().hashCode();
        result = 31 * result + getTestMethod().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TestExecutionResult{" +
                "testClass='" + testClass + '\'' +
                ", testMethod='" + testMethod + '\'' +
                ", runTime=" + runTime +
                ", wasSuccessful=" + wasSuccessful +
                '}';
    }
}
