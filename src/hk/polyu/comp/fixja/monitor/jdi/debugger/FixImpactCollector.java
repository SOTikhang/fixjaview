package hk.polyu.comp.fixja.monitor.jdi.debugger;

import com.sun.jdi.*;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.tools.example.debug.expr.ExpressionParser;
import hk.polyu.comp.fixja.fixaction.FixAction;
import hk.polyu.comp.fixja.fixaction.FixedMethodNameFormatter;
import hk.polyu.comp.fixja.fixer.BatchFixInstrumentor;
import hk.polyu.comp.fixja.fixer.config.FailureHandling;
import hk.polyu.comp.fixja.fixer.config.FixerOutput;
import hk.polyu.comp.fixja.fixer.log.LogLevel;
import hk.polyu.comp.fixja.fixer.log.LoggingService;
import hk.polyu.comp.fixja.java.JavaProject;
import hk.polyu.comp.fixja.monitor.ExpressionToMonitor;
import hk.polyu.comp.fixja.monitor.MethodToMonitor;
import hk.polyu.comp.fixja.monitor.state.DebuggerEvaluationResult;
import hk.polyu.comp.fixja.monitor.state.ProgramState;
import hk.polyu.comp.fixja.tester.TesterConfig;

import java.nio.file.Path;
import java.util.*;

import static hk.polyu.comp.fixja.fixer.log.LoggingService.shouldLogDebug;

public class FixImpactCollector extends AbstractDebuggerLauncher {

    private int maxFailingTests;
    private List<FixAction> fixActions;
    private int fixActionIndex;
    private FixAction currentFixAction;
    private boolean shouldMonitorExitState;

    private com.sun.jdi.Location mtfEntryLocation;
    private com.sun.jdi.Location mtfExitLocation;
    private List<BreakpointRequest> breakpointRequestsForMonitoring;
    private int nbrStackFramesAtMethodEntry;
    private ProgramState exitState;

    public FixImpactCollector(JavaProject project,
                              LogLevel logLevel,
                              int maxFailingTests,
                              long timeoutPerTest,
                              FailureHandling failureHandling,
                              boolean shouldExitUponKilledTest) {
        super(project, logLevel, timeoutPerTest, failureHandling, shouldExitUponKilledTest);

        this.maxFailingTests = maxFailingTests;
    }

    public void validateFixAction(List<FixAction> fixActions, int fixActionIndex, boolean shouldMonitorExitState){
        this.fixActions = fixActions;
        this.fixActionIndex = fixActionIndex;
        this.shouldMonitorExitState = shouldMonitorExitState;

        boolean hasError = false;
        String msg = "";
        currentFixAction = this.fixActions.get(this.fixActionIndex);

        if(shouldLogDebug()){
            LoggingService.debug("Ranking of fix #" + this.getCurrentFixAction().getFixId() + " starting...");
        }
        try {
            launch();
        }
        catch(Exception e){
            e.printStackTrace();
            hasError = true;
            msg = e.toString();
        }
        finally{
            if(shouldLogDebug()){
                if(hasError)
                    LoggingService.debug("Ranking of fix #" + currentFixAction.getFixId() + " failed with message: " + msg);
                else
                    LoggingService.debug("Ranking of fix #" + currentFixAction.getFixId() + " succeeded, "
                            + (currentFixAction.getSuccessfulTestExecutionResults().size() == getProject().getTestsToRun().size() ? "" : "not ")
                            + "valid");
            }
        }
    }

    // ================================ Getters and Setters

    public FixAction getCurrentFixAction() {
        return currentFixAction;
    }

    protected void prepareForLaunch(){
        newTestResults();
        this.currentFixAction.setTestExecutionResults(getResults());
    }

    protected String argumentsForTester() throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(TesterConfig.ACTIVE_FIX_INDEX_OPT).append(" ").append(this.fixActionIndex).append(" ")
                .append(TesterConfig.BATCH_SIZE_OPT).append(" ").append(BatchFixInstrumentor.BATCH_SIZE).append(" ")
                .append(TesterConfig.MAX_FAILING_TESTS_OPT).append(" ").append(this.maxFailingTests).append(" ")
                .append(TesterConfig.SHOULD_QUIT_UPON_MAX_FAILING_TESTS_OPT).append(" ").append(true).append(" ")
                .append(super.argumentsForTester());
        return sb.toString();
    }

    @Override
    protected void registerBreakpointForMonitoring(ReferenceType referenceType, boolean shouldEnable) throws AbsentInformationException {
        if(shouldMonitorExitState) {
            MethodToMonitor methodToMonitor = this.getCurrentFixAction().getLocation().getContextMethod();
            FixedMethodNameFormatter methodNameFormatter = new FixedMethodNameFormatter();
            String fixedMethodToMonitorName = methodNameFormatter.getFixedMethodName(methodToMonitor.getMethodAST().getName().getIdentifier(), this.fixActionIndex);

            List<Method> methods = referenceType.methodsByName(fixedMethodToMonitorName);
            if (methods.size() != 1) throw new IllegalStateException("Unexpected number of fixed method!");
            Method fixedMethod = methods.get(0);

            setMethodEntryAndExitLocation(fixedMethod);

            // Monitor only the entry and exit states of the method-to-fix
            breakpointRequestsForMonitoring = new LinkedList<>();
            breakpointRequestsForMonitoring.add(registerOneBreakpoint(mtfEntryLocation, shouldEnable));
            breakpointRequestsForMonitoring.add(registerOneBreakpoint(mtfExitLocation, shouldEnable));
        }
    }

    private void setMethodEntryAndExitLocation(Method method){
        try {
            List<com.sun.jdi.Location> locations = method.allLineLocations();
            for(com.sun.jdi.Location location: locations){
                if(mtfEntryLocation == null || mtfEntryLocation.lineNumber() > location.lineNumber())
                    mtfEntryLocation = location;
                if(mtfExitLocation == null || mtfExitLocation.lineNumber() < location.lineNumber())
                    mtfExitLocation = location;
            }
        }
        catch (AbsentInformationException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String testsToRunInString() throws Exception {
        return getProject().commandLineArgumentForTestsToRun();
    }

    protected Path getLogPath() {
        return FixerOutput.getFixLogFilePath(this.currentFixAction.getFixId());
    }

    @Override
    protected void debuggerFinished() {
    }

    protected void processTestStart(BreakpointEvent breakpointEvent) {
        super.processTestStart(breakpointEvent);

        nbrStackFramesAtMethodEntry = -1;

        if(this.shouldMonitorExitState) {
            if(breakpointRequestsForMonitoring != null) breakpointRequestsForMonitoring.forEach(x -> x.enable());
        }
    }

    protected void processTestEnd(BreakpointEvent breakpointEvent){
        super.processTestEnd(breakpointEvent);

        if(this.shouldMonitorExitState) {
            if(breakpointRequestsForMonitoring != null) breakpointRequestsForMonitoring.forEach(x -> x.disable());
        }
    }


    protected void processMonitorLocation(BreakpointEvent breakpointEvent) throws AbsentInformationException {
        if(shouldLogDebug()){
            LoggingService.debug("Fix validation at location " + breakpointEvent.location().lineNumber());
        }

        int lineNo = breakpointEvent.location().lineNumber();

        if(lineNo == mtfEntryLocation.lineNumber() && nbrStackFramesAtMethodEntry == -1) {
            nbrStackFramesAtMethodEntry = safeGetNbrStackFrames(breakpointEvent);
        }
        else if(lineNo == mtfExitLocation.lineNumber() && nbrStackFramesAtMethodEntry == safeGetNbrStackFrames(breakpointEvent)){
            nbrStackFramesAtMethodEntry = -1;

            Set<ExpressionToMonitor> expressionToMonitorSet = getProject().getMethodToMonitor().getMethodDeclarationInfoCenter()
                    .getExpressionsToMonitorAtMethodExit();

            ExpressionParser.GetFrame getFrame = new ExpressionParser.GetFrame() {
                public StackFrame get() throws IncompatibleThreadStateException {
                    return breakpointEvent.thread().frame(0);
                }
            };

            exitState = new ProgramState(getProject().getMethodToMonitor().getMethodDeclarationInfoCenter().getExitLocation());
            Set<ExpressionToMonitor> invalidExpressions = new HashSet<>();
            for (ExpressionToMonitor etm : expressionToMonitorSet) {

                DebuggerEvaluationResult debuggerEvaluationResult = evaluate(getVirtualMachine(), getFrame, etm);

                if(debuggerEvaluationResult.hasSyntaxError())
                    invalidExpressions.add(etm);

                if (!debuggerEvaluationResult.hasSemanticError() && !debuggerEvaluationResult.hasSyntaxError())
                    exitState.extend(etm, debuggerEvaluationResult);
            }
            expressionToMonitorSet.removeAll(invalidExpressions);

            getCurrentTestResult().addExitState(exitState.getExtendedState(getProject().getMethodToMonitor()
                    .getMethodDeclarationInfoCenter().getStateSnapshotExpressionsAtMethodExit()));
        }
    }


}
