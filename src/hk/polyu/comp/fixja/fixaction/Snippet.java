package hk.polyu.comp.fixja.fixaction;

import hk.polyu.comp.fixja.ast.ExpressionCollector;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.Statement;

import java.util.HashSet;
import java.util.Set;

import static hk.polyu.comp.fixja.util.CommonUtils.checkStmt;

/**
 * Created by Ls CHEN.
 */
public class Snippet {
    private String seed;
    private Statement initASTNode;
    private String statementString;
    private Set<Schemas.Schema> fixSchemas;

    public Snippet(ASTNode initASTNode, Set<Schemas.Schema> fixSchemas, String snippetStrategy, long snapshotID) {
        this.initASTNode = checkStmt(initASTNode);
        this.statementString = initASTNode.toString();
        this.fixSchemas = fixSchemas;
        this.seed = "strategy-" + snippetStrategy + ";; ssID-" + snapshotID;
    }

    public Snippet(String statementString, Set<Schemas.Schema> fixSchemas) {
        this.statementString = statementString;
        this.fixSchemas = fixSchemas;
    }

    public Statement getInitASTNode() {
        return initASTNode;
    }

//    private Set<ExpressionToMonitor> allSubExpCache;
//
//    public Set<ExpressionToMonitor> getAllSubExp() {
//        if (allSubExpCache == null) {
//            allSubExpCache = new HashSet<>();
//
//            ExpressionCollector collector = new ExpressionCollector(false);
//            collector.collect(getInitASTNode());
//            collector.getSubExpressionSet().stream()
//                    .filter(x -> !(x instanceof NumberLiteral))
//                    .forEach(x -> allSubExpCache.add(new ExpressionToMonitor(x, x.resolveTypeBinding())));
//        }
//        return allSubExpCache;
//    }

    private Set<String> subExpressionStrings;

    public Set<String> getAllSubExpStrings() {
        if (subExpressionStrings == null) {
            subExpressionStrings = new HashSet<>();

            ExpressionCollector collector = new ExpressionCollector(false);
            collector.collect(getInitASTNode());
            collector.getSubExpressionSet().stream()
                    .filter(x -> !(x instanceof NumberLiteral))
                    .forEach(x -> subExpressionStrings.add(x.toString()));
        }
        return subExpressionStrings;
    }

    public String getStatementString() {
        return statementString;
    }

    public Set<Schemas.Schema> getFixSchemas() {
        return fixSchemas;
    }

    public String getSeed() {
        return seed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Snippet snippet = (Snippet) o;

        if (!statementString.equals(snippet.statementString)) return false;
        return fixSchemas.equals(snippet.fixSchemas);
    }

    @Override
    public int hashCode() {
        int result = statementString.hashCode();
        result = 31 * result + fixSchemas.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Snippet{" +
                "'\n" + statementString + "\n   '" +
                ", Schemas=" + fixSchemas +
                ", seed=" + seed +
                '}';
    }
}
