package hk.polyu.comp.fixja.fixaction.strategy.preliminary;

import hk.polyu.comp.fixja.ast.ExpNodeFinder;
import hk.polyu.comp.fixja.ast.ExpressionCollector;
import hk.polyu.comp.fixja.fixaction.Snippet;
import hk.polyu.comp.fixja.fixaction.strategy.Strategy;
import hk.polyu.comp.fixja.fixaction.strategy.StrategyUtils;
import hk.polyu.comp.fixja.monitor.ExpressionToMonitor;
import hk.polyu.comp.fixja.util.CommonUtils;
import org.eclipse.jdt.core.dom.*;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.TextEdit;

import java.util.HashSet;
import java.util.Set;

import static hk.polyu.comp.fixja.util.CommonUtils.isAllConstant;
import static org.eclipse.jdt.core.dom.InfixExpression.Operator.EQUALS;
import static org.eclipse.jdt.core.dom.InfixExpression.Operator.NOT_EQUALS;
import static org.eclipse.jdt.core.dom.PrefixExpression.Operator.NOT;

/**
 * Created by Ls CHEN.
 */
public class Strategy4IfCondition extends Strategy {
    Set<Snippet> snippetSet;
    ITypeBinding type;


    @Override
    public Set<Snippet> process() {
        this.snippetSet = new HashSet<>();
        type = getStateSnapshot().getSnapshotExpression().getOperands().get(0).getType();
        Statement oldStmt = getStateSnapshot().getLocation().getStatement();
        if (oldStmt instanceof IfStatement) {
            IfStatement oldIfStmt = (IfStatement) oldStmt;
            ast = oldIfStmt.getRoot().getAST();
            templateMutateIfCond(oldIfStmt);
            templateDisableSubIfCond(oldIfStmt, true);
            templateDisableSubIfCond(oldIfStmt, false);
            if (shouldAppendIfCond(oldIfStmt)) {
                templateAppendSnapshotToIfCond(oldIfStmt);
            }
        }
        return snippetSet;
    }

    private boolean shouldAppendIfCond(IfStatement oldIfStmt) {
        ExpressionCollector collector = new ExpressionCollector(false);
        collector.collect(oldIfStmt.getExpression());
        Set<Expression> ifCondSubs = collector.getSubExpressionSet();
        Set<ExpressionToMonitor> snapshotSubs = getStateSnapshot().getSnapshotExpression().getSubExpressions();
        Set<ASTNode> aggregate = new HashSet<>();
        ASTMatcher matcher = new ASTMatcher();
        ifCondSubs.stream().forEach(ifSub -> {
            snapshotSubs.stream().forEach(ssSub -> {
                if (ifSub.subtreeMatch(matcher, ssSub.getExpressionAST()))
                    aggregate.add(ifSub);
            });
        });
        if (aggregate.size() != 2 && !isAllConstant(aggregate))
            return true;
        return false;
    }

    private void templateDisableSubIfCond(IfStatement oldIfStmt, boolean replaceWith) {
        IfStatement newIfStmt = (IfStatement) ASTNode.copySubtree(ast, oldIfStmt);
        ASTParser oldExpAstParser = ASTParser.newParser(AST.JLS8);
        oldExpAstParser.setSource(newIfStmt.getExpression().toString().toCharArray());
        oldExpAstParser.setKind(ASTParser.K_EXPRESSION);
        Expression oldExp = (Expression) oldExpAstParser.createAST(null);
//        Expression oldExp = newIfStmt.getExpression();
        ASTRewrite rewrite = ASTRewrite.create(oldExp.getAST());

        ExpNodeFinder findNodeByExp = new ExpNodeFinder(oldExp, getStateSnapshot().getSnapshotExpression().getExpressionAST());
        Expression toDisable = (Expression) findNodeByExp.find();

        if (toDisable != null) {//如果Old_if_cond 中含有snapshot EXP，则将其替换为T/F（和simi==1是等价的）
            Document document = new Document(oldExp.toString());
            rewrite.replace(toDisable, ast.newBooleanLiteral(replaceWith), null);
            TextEdit edits = rewrite.rewriteAST(document, null);
            try {
                edits.apply(document);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
            String newIfCondStr = document.get();
            ASTParser newExpAstParser = ASTParser.newParser(AST.JLS8);
            newExpAstParser.setSource(newIfCondStr.toCharArray());
            newExpAstParser.setKind(ASTParser.K_EXPRESSION);
            Expression newIfCond = (Expression) newExpAstParser.createAST(null);
            newIfStmt.setExpression((Expression) ASTNode.copySubtree(ast, newIfCond));
            snippetSet.add(new Snippet(newIfStmt, StrategyUtils.fitSchemaE, getStrategyName("if_cond-remove"), getStateSnapshot().getID()));
        }
    }

    private void templateAppendSnapshotToIfCond(IfStatement oldIfStmt) {
        IfStatement newIfStmt = (IfStatement) ASTNode.copySubtree(ast, oldIfStmt);
        Expression oldExp = newIfStmt.getExpression();
        InfixExpression newExp = ast.newInfixExpression();
        newExp.setLeftOperand((Expression) ASTNode.copySubtree(ast, oldExp));
        newExp.setOperator(InfixExpression.Operator.CONDITIONAL_AND);
        newExp.setRightOperand((Expression) ASTNode.copySubtree(ast, getStateSnapshot().getSnapshotExpression().getExpressionAST()));
        newIfStmt.setExpression(newExp);
        snippetSet.add(new Snippet(newIfStmt, StrategyUtils.fitSchemaE, getStrategyName("if_cond && snapshot"), getStateSnapshot().getID()));

        IfStatement newIfStmt1 = (IfStatement) ASTNode.copySubtree(ast, oldIfStmt);
        InfixExpression newExp1 = ast.newInfixExpression();
        newExp1.setLeftOperand((Expression) ASTNode.copySubtree(ast, oldExp));
        newExp1.setOperator(InfixExpression.Operator.CONDITIONAL_OR);
        newExp1.setRightOperand((Expression) ASTNode.copySubtree(ast, getStateSnapshot().getSnapshotExpression().getExpressionAST()));
        newIfStmt1.setExpression(newExp1);
        snippetSet.add(new Snippet(newIfStmt1, StrategyUtils.fitSchemaE, getStrategyName("if_cond || snapshot"), getStateSnapshot().getID()));
    }

    private void templateMutateIfCond(IfStatement oldIfStmt) {

        Expression oldExp = oldIfStmt.getExpression();
        CollectOperand collector = new CollectOperand();
        Set<Expression> operandSet = collector.collect(oldExp);

        operandSet.stream().forEach(operand -> {
            mutate(operand, oldIfStmt);
        });
    }

    /**
     * 对每个subIfCond进行mutate
     *
     * @param exp
     * @param oldIfStmt
     */
    private void mutate(Expression exp, IfStatement oldIfStmt) {
        Set<Expression> mutatedExpSet = new HashSet<>();
        mutatedExpSet.add(mutateNegation(exp));
        if (exp instanceof InfixExpression) {
            mutatedExpSet.addAll(mutateInfixOperator((InfixExpression) exp));
        } else if (exp instanceof PrefixExpression) {
        } else if (exp instanceof MethodInvocation) {
        } else if (exp instanceof SimpleName) {
        }
        mutatedExpSet.stream().forEach(mutatedExp -> {
            replaceSubIfCond(exp, mutatedExp, oldIfStmt);
        });
    }

    private void replaceSubIfCond(Expression oldSubExp, Expression newSubExp, IfStatement oldIfStmt) {

        IfStatement newIfStmt = (IfStatement) ASTNode.copySubtree(ast, oldIfStmt);
        Expression oldIfCond = newIfStmt.getExpression();
//        ASTRewrite rewrite = ASTRewrite.create(oldIfCond.getAST());//直接用这个AST会导致rewrite的edits不修改document

        ASTParser ifCondParser = ASTParser.newParser(AST.JLS8);
        ifCondParser.setSource(oldIfCond.toString().toCharArray());
        ifCondParser.setKind(ASTParser.K_EXPRESSION);
        Expression old_if_cond_ast = (Expression) ifCondParser.createAST(null);
        ASTRewrite rewriter = ASTRewrite.create(old_if_cond_ast.getAST());

        ExpNodeFinder findNodeByExp = new ExpNodeFinder(old_if_cond_ast, oldSubExp);
        Expression toReplace = (Expression) findNodeByExp.find();

        if (toReplace != null) {
            Document document = new Document(old_if_cond_ast.toString());
            rewriter.replace(toReplace, newSubExp, null);
            TextEdit edits = rewriter.rewriteAST(document, null);
            try {
                edits.apply(document);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
            String newIfCondStr = document.get();
            ASTParser astParser = ASTParser.newParser(AST.JLS8);
            astParser.setSource(newIfCondStr.toCharArray());
            astParser.setKind(ASTParser.K_EXPRESSION);
            Expression newIfCond = (Expression) astParser.createAST(null);
            newIfStmt.setExpression((Expression) ASTNode.copySubtree(ast, newIfCond));
            snippetSet.add(new Snippet(newIfStmt, StrategyUtils.fitSchemaE, getStrategyName("if_cond-mutate"), getStateSnapshot().getID()));
        }
    }

    private Expression mutateNegation(Expression exp) {
        if (exp instanceof InfixExpression) {
            InfixExpression cp_old_infix = (InfixExpression) ASTNode.copySubtree(ast, exp);
            InfixExpression.Operator op = cp_old_infix.getOperator();
            if (cp_old_infix != null && op != null && (op == EQUALS || op == NOT_EQUALS)) {
                if (op == EQUALS)
                    cp_old_infix.setOperator(NOT_EQUALS);
                else
                    cp_old_infix.setOperator(EQUALS);
                return cp_old_infix;
            }
        } else if (exp instanceof PrefixExpression) {
            PrefixExpression oldPre = (PrefixExpression) exp;
            if (oldPre.getOperator().equals(PrefixExpression.Operator.NOT)) {
                return (Expression) ASTNode.copySubtree(ast, oldPre.getOperand());
            }
        }
        PrefixExpression prefixExpression = ast.newPrefixExpression();
        prefixExpression.setOperand(CommonUtils.checkParenthesizeNeeded(exp));
        prefixExpression.setOperator(NOT);
        return prefixExpression;

    }

    private Set<Expression> mutateInfixOperator(InfixExpression infixExp) {
        Set<Expression> mutatedExpSet = new HashSet<>();
        InfixExpression.Operator op = infixExp.getOperator();
        if (op.equals(InfixExpression.Operator.GREATER)) {
            InfixExpression mutateExp = (InfixExpression) ASTNode.copySubtree(ast, infixExp);
            mutateExp.setOperator(InfixExpression.Operator.GREATER_EQUALS);
            mutatedExpSet.add(mutateExp);
        } else if (op.equals(InfixExpression.Operator.GREATER_EQUALS)) {
            InfixExpression mutateExp = (InfixExpression) ASTNode.copySubtree(ast, infixExp);
            mutateExp.setOperator(InfixExpression.Operator.GREATER);
            mutatedExpSet.add(mutateExp);
        } else if (op.equals(InfixExpression.Operator.LESS)) {
            InfixExpression mutateExp = (InfixExpression) ASTNode.copySubtree(ast, infixExp);
            mutateExp.setOperator(InfixExpression.Operator.LESS_EQUALS);
            mutatedExpSet.add(mutateExp);
        } else if (op.equals(InfixExpression.Operator.LESS_EQUALS)) {
            InfixExpression mutateExp = (InfixExpression) ASTNode.copySubtree(ast, infixExp);
            mutateExp.setOperator(InfixExpression.Operator.LESS);
            mutatedExpSet.add(mutateExp);
        } else if (op.equals(InfixExpression.Operator.EQUALS)) {
            if (!infixExp.getLeftOperand().resolveTypeBinding().isPrimitive()) {
                MethodInvocation mutateExp = ast.newMethodInvocation();
                mutateExp.setExpression((Expression) ASTNode.copySubtree(ast, infixExp.getLeftOperand()));
                mutateExp.setName(ast.newSimpleName("equals"));
                mutateExp.arguments().add((Expression) ASTNode.copySubtree(ast, infixExp.getRightOperand()));
                mutatedExpSet.add(mutateExp);
            }
        }
        return mutatedExpSet;
    }

    private class CollectOperand {
        Set<Expression> operandSet;

        public CollectOperand() {
            this.operandSet = new HashSet<>();
        }

        public Set<Expression> collect(Expression root) {
            getOperand(root);
            return operandSet;
        }

        private void getOperand(Expression currentExp) {

            if (currentExp instanceof InfixExpression) {
                InfixExpression infixCurrent = (InfixExpression) currentExp;
                if (infixCurrent.getOperator().equals(InfixExpression.Operator.CONDITIONAL_AND) ||
                        infixCurrent.getOperator().equals(InfixExpression.Operator.CONDITIONAL_OR)) {
                    getOperand(infixCurrent.getLeftOperand());
                    getOperand(infixCurrent.getRightOperand());
                    for (Object o : infixCurrent.extendedOperands()) {
                        getOperand((Expression) o);
                    }
                } else operandSet.add(currentExp);
            } else if (currentExp instanceof PrefixExpression) {
                PrefixExpression prefixCurrent = (PrefixExpression) currentExp;
                getOperand(prefixCurrent.getOperand());
                operandSet.add(currentExp);
            } else if (currentExp instanceof ParenthesizedExpression)
                getOperand(((ParenthesizedExpression) currentExp).getExpression());
            else operandSet.add(currentExp);
        }
    }

}
