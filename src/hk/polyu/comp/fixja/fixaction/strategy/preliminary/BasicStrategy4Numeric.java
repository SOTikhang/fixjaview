package hk.polyu.comp.fixja.fixaction.strategy.preliminary;

import hk.polyu.comp.fixja.ast.ASTUtils4SelectInvocation;
import hk.polyu.comp.fixja.monitor.ExpressionToMonitor;
import org.eclipse.jdt.core.dom.*;

import static hk.polyu.comp.fixja.fixaction.strategy.StrategyUtils.constructAssignment;
import static hk.polyu.comp.fixja.fixaction.strategy.StrategyUtils.getReplaceVarName;

/**
 * Created by Ls CHEN
 */
public class BasicStrategy4Numeric extends AbsBasicStrategy {


    @Override
    boolean isDesiredType() {
        return getStateSnapshot().getSnapshotExpression().getOperands().get(0).isNumericType();
    }

    /**
     * Build snippetMap for a binary snapshot expression.
     *
     * @return
     * @throws Exception
     */
    void building(ExpressionToMonitor leftOperand, ExpressionToMonitor rightOperand) {
        templateAssignR2L(leftOperand, rightOperand);
        templateAssignR2L(rightOperand, leftOperand);
    }

    void building(ExpressionToMonitor operand) {
        templateAssignInvoke(operand);
    }


    private void templateSelfIncrement(ExpressionToMonitor operand) {
        String strategyName = getStrategyName("o++");
        if (operand.isValidVariable()) {
            PostfixExpression postfixExpression = ast.newPostfixExpression();
            postfixExpression.setOperator(PostfixExpression.Operator.INCREMENT);
            postfixExpression.setOperand((Expression) ASTNode.copySubtree(ast, operand.getExpressionAST()));
            createSnippet(postfixExpression, strategyName);
        } else {
            SimpleName tmp_exp = ast.newSimpleName(getReplaceVarName(operand));
            PostfixExpression postfixExpression = ast.newPostfixExpression();
            postfixExpression.setOperator(PostfixExpression.Operator.INCREMENT);
            postfixExpression.setOperand(tmp_exp);
            createSnippet(operand, tmp_exp, postfixExpression, strategyName);
        }
    }

    private void templateSelfDecrement(ExpressionToMonitor operand) {
        String strategyName = getStrategyName("o--");
        if (operand.isValidVariable()) {
            PostfixExpression postfixExpression = ast.newPostfixExpression();
            postfixExpression.setOperator(PostfixExpression.Operator.DECREMENT);
            postfixExpression.setOperand((Expression) ASTNode.copySubtree(ast, operand.getExpressionAST()));
            createSnippet(postfixExpression, strategyName);
        } else {
            SimpleName tmp_exp = ast.newSimpleName(getReplaceVarName(operand));
            PostfixExpression postfixExpression = ast.newPostfixExpression();
            postfixExpression.setOperator(PostfixExpression.Operator.DECREMENT);
            postfixExpression.setOperand(tmp_exp);
            createSnippet(operand, tmp_exp, postfixExpression, strategyName);
        }
    }

    private void templateAssignNumberLiteral(ExpressionToMonitor operand) {
        NumberLiteral one = ast.newNumberLiteral("1");
        NumberLiteral zero = ast.newNumberLiteral("0");
        PrefixExpression minusOne = ast.newPrefixExpression();
        minusOne.setOperator(PrefixExpression.Operator.MINUS);
        minusOne.setOperand(ast.newNumberLiteral("1"));
        assignNumberLiteral(operand, one);
        assignNumberLiteral(operand, zero);
        assignNumberLiteral(operand, minusOne);

    }

    private void assignNumberLiteral(ExpressionToMonitor operand, Expression numberLiteral) {
        String strategyName = getStrategyName("o=" + numberLiteral.toString());
        if (operand.isValidVariable()) {
            Expression constructedExpT = constructAssignment(operand.getExpressionAST(), numberLiteral);
            createSnippet(constructedExpT, strategyName);
        } else {
            SimpleName tmp_exp = ast.newSimpleName(getReplaceVarName(operand));
            Expression constructedExpT = constructAssignment(tmp_exp, numberLiteral);
            createSnippet(operand, tmp_exp, constructedExpT, strategyName);
        }
    }

    private void templateAssignInvoke(ExpressionToMonitor operand) {
        String strategyName = getStrategyName("o=valid_invoke");
        for (MethodInvocation invocation : ASTUtils4SelectInvocation.assignableStateInvocation1(true, getStateSnapshot().getLocation())) {
            constructAndCreate(operand, invocation, strategyName);
        }
    }

    private void templateAssignR2L(ExpressionToMonitor left, ExpressionToMonitor right) {
        if (left.isValidVariable()) {
            Expression constructedExp = constructAssignment(left.getExpressionAST(), right.getExpressionAST());
            createSnippet(constructedExp, getStrategyName("l=r"));
        }
    }

    private void templateAssignLpR2L(ExpressionToMonitor left, ExpressionToMonitor right) {
        if (left.isValidVariable()) {
            InfixExpression n_right = ast.newInfixExpression();
            n_right.setOperator(InfixExpression.Operator.PLUS);
            n_right.setLeftOperand((Expression) ASTNode.copySubtree(ast, left.getExpressionAST()));
            n_right.setRightOperand((Expression) ASTNode.copySubtree(ast, right.getExpressionAST()));
            Expression constructedExp = constructAssignment(left.getExpressionAST(), n_right);
            createSnippet(constructedExp, getStrategyName("l=l+r"));
        }
    }

    private void templateAssignLmR2L(ExpressionToMonitor left, ExpressionToMonitor right) {
        if (left.isValidVariable()) {
            InfixExpression n_right = ast.newInfixExpression();
            n_right.setOperator(InfixExpression.Operator.MINUS);
            n_right.setLeftOperand((Expression) ASTNode.copySubtree(ast, left.getExpressionAST()));
            n_right.setRightOperand((Expression) ASTNode.copySubtree(ast, right.getExpressionAST()));
            Expression constructedExp = constructAssignment(left.getExpressionAST(), n_right);
            createSnippet(constructedExp, getStrategyName("l=l-r"));
        }
    }

}
