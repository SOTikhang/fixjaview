package fixjaview.views;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.eclipse.compare.IModificationDate;
import org.eclipse.compare.IStreamContentAccessor;
import org.eclipse.compare.ITypedElement;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.graphics.Image;

public class CompareItem implements IStreamContentAccessor, ITypedElement, IModificationDate {

	private String contents, name;
	private long time;

	CompareItem(String name, String contents) {
		this.name = name;
		this.contents = contents;
	}

	@Override
	public InputStream getContents() throws CoreException {
		return new ByteArrayInputStream(contents.getBytes());
	}

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public long getModificationDate() {
		return time;
	}

	@Override
	public String getName() {
		return name;
	}

	public String getString() {
		return contents;
	}

	@Override
	public String getType() {
		return ITypedElement.TEXT_TYPE;
	}

}
