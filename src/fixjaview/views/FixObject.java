package fixjaview.views;

public class FixObject {
	int fixID;
	String seed="";
	String fixCode="";
	String location="";
	String score="";
	String sim="";
	
	public int getFixID() {
		return fixID;
	}
	public void setFixID(int fixID) {
		this.fixID = fixID;
	}
	public String getSeed() {
		return seed;
	}
	public void setSeed(String seed) {
		this.seed = seed;
	}
	public String getFixCode() {
		return fixCode;
	}
	public void setFixCode(String fixCode) {
		this.fixCode = fixCode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getSim() {
		return sim;
	}
	public void setSim(String sim) {
		this.sim = sim;
	}

}