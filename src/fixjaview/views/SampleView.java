package fixjaview.views;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.CompareEditorInput;
import org.eclipse.compare.CompareUI;
import org.eclipse.compare.structuremergeviewer.DiffNode;
import org.eclipse.compare.structuremergeviewer.Differencer;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.part.ViewPart;

import fixjaview.RunFixja;
import hk.polyu.comp.fixja.fixaction.FixAction;
import hk.polyu.comp.fixja.fixer.Application;
import hk.polyu.comp.fixja.fixer.config.FixerOutput;
import hk.polyu.comp.fixja.monitor.LineLocation;
import org.eclipse.swt.widgets.Label;

import static hk.polyu.comp.fixja.fixer.log.LoggingService.shouldLogDebug;

import java.awt.Dialog;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.custom.ViewForm;





/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 */

public class SampleView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "fixjaview.views.SampleView";

	private static String beforeFixCode = "";
	private static String selectedFixCode = "";

	private IMemento memento;

	private String header1String = "# JDK environment";
	private String header2String = "# FixJa settings";
	private String header3String = "# Project specific settings";
	private String header4String = "# Format: RelativePath1; RelativePath2";
	private String header5String = "# Format: RelativePath1; RelativePath2";
	private String header6String = "# Format: ClassName1::MethodName1;ClassName2::MethodName2";
	private String header7String = "# Format: ClassName1::MethodName1;ClassName2::MethodName2";

	private Text JDKFormText;
	private Text logFileText;
	private Text logLevelText;
	private Text projectRootDirText;
	private Text projectSourceDirText;
	private Text projectOutputDirText;
	private Text projectLibText;
	private Text projectTestSourceDirText;
	private Text projectTestOutputDirText;
	private Text projectTestsToIncludeText;
	private Text projectTestsToExcludeText;
	private Text projectExtraClasspathText;
	private Text projectCompilationCommandText;
	private Text projectExecutionCommandText;
	private Text methodToFixText;

	private static List<String> fixIdList = new ArrayList<String>();
	private static List<String> seedList = new ArrayList<String>();
	private static List<String> simList = new ArrayList<String>();
	private static List<String> scoreList = new ArrayList<String>();
	private static List<String> fixCodeList = new ArrayList<String>();
	private static List<String> locationList = new ArrayList<String>();
	private static List<FixObject> FixObjectList = new ArrayList<FixObject>();

	private int runJaidCount = 0;

	// Fix Tab variable
	private static GridData textField = new GridData();
	private static GridData treeField = new GridData();
	Font font = new Font(null, "Tahoma", 11, 0);


	// for display red color
	final Display display = Display.getDefault();

	static boolean runJAIDDone = false;

	String JDKPath = "V:\\jdk1.8.0_101\\bin\\java.exe";
	String runtimeEclipseWSPath = "V:/FYP/runtime-EclipseApplication";
	String testCaseProjectPath = "V:/FYP/runtime-EclipseApplication/AfTest";
	private Text textJdk;
	private Text textLogFilePath;
	private Text textPjRootPath;
	private Text textPjSourcePath;
	private Text textPjOutputPath;
	
	
	private static BufferedReader br = null;	

	
	//create view
	static Composite composite = null;
	static Composite composite_1 = null;
	// Tab3
	static Composite composite3= null;
	GridLayout gridLayout = new GridLayout();

	static int countReceiver = 0;
	

	/*
	 * The content provider class is responsible for providing objects to the view.
	 * It can wrap existing objects in adapters or simply return objects as-is.
	 * These objects may be sensitive to the current input of the view, or ignore it
	 * and always show the same content (like Task List, for example).
	 */

	class ViewContentProvider implements IStructuredContentProvider {
		@Override
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public Object[] getElements(Object parent) {
			return new String[] { "One", "Two", "Three" };
		}
	}

	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}

		@Override
		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}

		@Override
		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}
	}

	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public SampleView() {
		gridLayout.numColumns = 2;

	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	@Override

	public void createPartControl(Composite parent) {
//		oldCreatePartControl(parent);
		newCreatePartControl(parent);

	};
 
	
	public void oldCreatePartControl(Composite parent) {


		final TabFolder tabFolder1 = new TabFolder(parent, SWT.BORDER);

		TabItem tabItem1 = new TabItem(tabFolder1, SWT.NONE);
		tabItem1.setText("Settings");

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;

		// Tab1
		ScrolledComposite sc = new ScrolledComposite(tabFolder1, SWT.H_SCROLL | SWT.V_SCROLL);
		Composite composite = new Composite(sc, SWT.MULTI);
		composite.setLayout(gridLayout);
//		// Tab2 					
//		Composite composite2= new Composite(tabFolder1, SWT.MULTI);
//		TabItem tabItem2 = new TabItem(tabFolder1, SWT.NONE);
//		tabItem2.setText("Faults");
//		tabItem2.setControl(composite2);
		// Tab3
		Composite composite3 = new Composite(tabFolder1, SWT.MULTI);
		composite3 = new Composite(tabFolder1, SWT.MULTI);

		TabItem tabItem3 = new TabItem(tabFolder1, SWT.NONE);
		tabItem3.setText("Fixes");
		tabItem3.setControl(composite3);

		readPlausibleFixActions(composite3);

//		// Tab4
//		Composite composite4 = new Composite(tabFolder1, SWT.MULTI);
//		TabItem tabItem4 = new TabItem(tabFolder1, SWT.NONE);						
//		tabItem4.setText("Output");
//		tabItem4.setControl(composite4);

		// Tab 1 Content
		// textfield width
		GridData textField = new GridData(500, SWT.DEFAULT);
//		textField.widthHint = 500;
		// bold text
		StyleRange boldStyle = new StyleRange();
		boldStyle.start = 0;
		boldStyle.fontStyle = SWT.BOLD;
		// red text
		StyleRange redTextStyle = new StyleRange();
		redTextStyle.start = 0;
		redTextStyle.fontStyle = SWT.ITALIC;

		Button btn1 = new Button(composite, SWT.PUSH);
		btn1.setText("Run JAID");
		
		Button loadBtn = new Button(composite, SWT.PUSH);
		loadBtn.setText("Load");
		loadBtn.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				loadProperties();
			}
		});

		StyledText redTextNotice = new StyledText(composite, SWT.PUSH);
		String redTextNoticeText = "Field(s) in blue is/are optional";
		redTextStyle.length = redTextNoticeText.length();
		redTextStyle.foreground = display.getSystemColor(SWT.COLOR_BLUE);
		redTextNotice.setText(redTextNoticeText);
		redTextNotice.setStyleRange(redTextStyle);

		Group JDKInfo = new Group(composite, SWT.NONE);
		JDKInfo.setText(header1String);
		JDKInfo.setFont(font);
		JDKInfo.setLayout(gridLayout);

//		StyledText header1 = new StyledText(JDKInfo, SWT.PUSH);
//		boldStyle.length = header1String.length();
//		header1.setText(header1String);
//		header1.setStyleRange(boldStyle);

		new Text(JDKInfo, SWT.PUSH).setText("JDKDir = ");
		JDKFormText = new Text(JDKInfo, SWT.BORDER);
		String javaHomewithJRE = System.getProperty("java.home");
		String javaHomepath = javaHomewithJRE.replace("\\jre", "");

		JDKFormText.setText(javaHomepath);
		JDKFormText.setLayoutData(new GridData(500, SWT.DEFAULT));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		

		Group fixJaSettingsInfo = new Group(composite, SWT.NONE);
		fixJaSettingsInfo.setText(header2String);
		fixJaSettingsInfo.setFont(font);
		fixJaSettingsInfo.setLayout(gridLayout);

		new Text(fixJaSettingsInfo, SWT.PUSH).setText("LogFile = ");
		logFileText = new Text(fixJaSettingsInfo, SWT.BORDER);
		logFileText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("logFile") != null)
			logFileText.setText(memento.getString("logFile"));

		new Text(fixJaSettingsInfo, SWT.PUSH).setText("LogLevel = ");
		logLevelText = new Text(fixJaSettingsInfo, SWT.BORDER);
		logLevelText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("logLevel") != null)
			logLevelText.setText(memento.getString("logLevel"));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");

//		StyledText header2 = new StyledText(composite, SWT.PUSH);
//		boldStyle.length = header2String.length();
//		header2.setText(header2String);
//		header2.setStyleRange(boldStyle);
//		new Text(composite, SWT.PUSH).setText("");

		Group projectSpecificInfo = new Group(composite, SWT.NONE);
		projectSpecificInfo.setText(header3String);
		projectSpecificInfo.setFont(font);
		projectSpecificInfo.setLayout(gridLayout);

		new Text(projectSpecificInfo, SWT.PUSH).setText("ProjectRootDir = ");
		projectRootDirText = new Text(projectSpecificInfo, SWT.BORDER);
		projectRootDirText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectRootDir") != null)
			projectRootDirText.setText(memento.getString("projectRootDir"));

		new Text(projectSpecificInfo, SWT.PUSH).setText("ProjectSourceDir = ");
		projectSourceDirText = new Text(projectSpecificInfo, SWT.BORDER);
		projectSourceDirText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectSourceDir") != null)
			projectSourceDirText.setText(memento.getString("projectSourceDir"));

		new Text(projectSpecificInfo, SWT.PUSH).setText("ProjectOutputDir = ");
		projectOutputDirText = new Text(projectSpecificInfo, SWT.BORDER);
		projectOutputDirText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectOutputDir") != null)
			projectOutputDirText.setText(memento.getString("projectOutputDir"));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");

		Group format1Info = new Group(composite, SWT.NONE);
		format1Info.setText(header4String);
		format1Info.setFont(font);
		format1Info.setLayout(gridLayout);

		new Text(format1Info, SWT.PUSH).setText("ProjectLib = ");
		projectLibText = new Text(format1Info, SWT.BORDER);
		projectLibText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectLib") != null)
			projectLibText.setText(memento.getString("projectLib"));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");

		Group format2Info = new Group(composite, SWT.NONE);
		format2Info.setText(header5String);
		format2Info.setFont(font);
		format2Info.setLayout(gridLayout);

		new Text(format2Info, SWT.PUSH).setText("ProjectTestSourceDir = ");
		projectTestSourceDirText = new Text(format2Info, SWT.BORDER);
		projectTestSourceDirText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectTestSourceDir") != null)
			projectTestSourceDirText.setText(memento.getString("projectTestSourceDir"));

		new Text(format2Info, SWT.PUSH).setText("ProjectTestOutputDir = ");
		projectTestOutputDirText = new Text(format2Info, SWT.BORDER);
		projectTestOutputDirText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectTestOutputDir") != null)
			projectTestOutputDirText.setText(memento.getString("projectTestOutputDir"));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");

		Group format3Info = new Group(composite, SWT.NONE);
		format3Info.setText(header6String);
		format3Info.setFont(font);
		format3Info.setLayout(gridLayout);

		StyledText optProjectTestsToInclude = new StyledText(format3Info, SWT.PUSH);
		String optProjectTestsToIncludeText = "ProjectTestsToInclude =";
		redTextStyle.length = optProjectTestsToIncludeText.length();
		redTextStyle.foreground = display.getSystemColor(SWT.COLOR_BLUE);
		optProjectTestsToInclude.setText(optProjectTestsToIncludeText);
		optProjectTestsToInclude.setStyleRange(redTextStyle);
		projectTestsToIncludeText = new Text(format3Info, SWT.BORDER);
		projectTestsToIncludeText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectTestsToInclude") != null)
			projectTestsToIncludeText.setText(memento.getString("projectTestsToInclude"));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");

		Group format4Info = new Group(composite, SWT.NONE);
		format4Info.setText(header7String);
		format4Info.setFont(font);
		format4Info.setLayout(gridLayout);

		StyledText optProjectTestsToExclude = new StyledText(format4Info, SWT.PUSH);
		String optProjectTestsToExcludeText = "ProjectTestsToExclude =";
		redTextStyle.length = optProjectTestsToExcludeText.length();
		redTextStyle.foreground = display.getSystemColor(SWT.COLOR_BLUE);
		optProjectTestsToExclude.setText(optProjectTestsToExcludeText);
		optProjectTestsToExclude.setStyleRange(redTextStyle);
		projectTestsToExcludeText = new Text(format4Info, SWT.BORDER);
		projectTestsToExcludeText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectTestsToExclude") != null)
			projectTestsToExcludeText.setText(memento.getString("projectTestsToExclude"));

		StyledText optProjectExtraClasspath = new StyledText(format4Info, SWT.PUSH);
		String optProjectExtraClasspathText = "ProjectExtraClasspath =";
		redTextStyle.length = optProjectExtraClasspathText.length();
		redTextStyle.foreground = display.getSystemColor(SWT.COLOR_BLUE);
		optProjectExtraClasspath.setText(optProjectExtraClasspathText);
		optProjectExtraClasspath.setStyleRange(redTextStyle);
		projectExtraClasspathText = new Text(format4Info, SWT.BORDER);
		projectExtraClasspathText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectExtraClasspath") != null)
			projectExtraClasspathText.setText(memento.getString("projectExtraClasspath"));

		StyledText optProjectCompilationCommand = new StyledText(format4Info, SWT.PUSH);
		String optProjectCompilationCommandText = "ProjectCompilationCommand =";
		redTextStyle.length = optProjectCompilationCommandText.length();
		redTextStyle.foreground = display.getSystemColor(SWT.COLOR_BLUE);
		optProjectCompilationCommand.setText(optProjectCompilationCommandText);
		optProjectCompilationCommand.setStyleRange(redTextStyle);
		projectCompilationCommandText = new Text(format4Info, SWT.BORDER);
		projectCompilationCommandText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectCompilationCommand") != null)
			projectCompilationCommandText.setText(memento.getString("projectCompilationCommand"));

		StyledText optProjectExecutionCommand = new StyledText(format4Info, SWT.PUSH);
		String optProjectExecutionCommandText = "ProjectExecutionCommand =";
		redTextStyle.length = optProjectExecutionCommandText.length();
		redTextStyle.foreground = display.getSystemColor(SWT.COLOR_BLUE);
		optProjectExecutionCommand.setText(optProjectExecutionCommandText);
		optProjectExecutionCommand.setStyleRange(redTextStyle);
		projectExecutionCommandText = new Text(format4Info, SWT.BORDER);
		projectExecutionCommandText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("projectExecutionCommand") != null)
			projectExecutionCommandText.setText(memento.getString("projectExecutionCommand"));

		new Text(format4Info, SWT.PUSH).setText("MethodToFix = ");
		methodToFixText = new Text(format4Info, SWT.BORDER);
		methodToFixText.setLayoutData(new GridData(500, SWT.DEFAULT));
		if (memento.getString("methodToFix") != null)
			methodToFixText.setText(memento.getString("methodToFix"));

		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");
		new Text(composite, SWT.PUSH).setText("");

		// InitProject
		initProperties();

		Button saveBtn1 = new Button(composite, SWT.PUSH);
		saveBtn1.setText("Save");
		saveBtn1.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				final Shell shell = new Shell(display);
				switch (e.type) {
				case SWT.Selection:
					if (checkValidSettings()) {
						saveProperites();
						MessageDialog.openInformation(shell, "Info", "Save successfully");
					} else {
						MessageDialog.openError(shell, "Error", "Please ensure to input All the required settings");
					}
					break;
				}
			}
		});

	

		btn1.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				final Shell shell = new Shell(display);
				if (checkValidSettings()) {
					saveProperites();
					// Run java project
					try {
						System.out.println("**********");
						System.out.println("**********");
						// Execute JAID
						runProcess(JDKPath + " -cp " + runtimeEclipseWSPath + "/fixja/lib/*;" + runtimeEclipseWSPath
								+ "/fixja/bin -ea hk/polyu/comp/fixja/fixer/Application --FixjaSettingFile "
								+ runtimeEclipseWSPath + "/fixja/misc/project.properties");
						runJAIDDone = true;
						if (runJaidCount == 0 && fixIdList.isEmpty()) {
							readPlausibleFixActions(composite);
							runJaidCount++;
						}
						MessageDialog.openInformation(shell, "Info", "JAID run successfully");
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} else {
					MessageDialog.openError(shell, "Error", "Please ensure to input All the required settings");
				}
			}
		});

		sc.setContent(composite);
		// Set the minimum size
		sc.setMinSize(1024, 768);
		// Expand both horizontally and vertically
		sc.setExpandHorizontal(true);
		sc.setExpandVertical(true);

		tabItem1.setControl(sc);

	}
	
	
public void newCreatePartControl(Composite parent) {
	
	System.out.println("newCreatePartControlnewCreatePartControlnewCreatePartControlnewCreatePartControl");
		
		TabFolder tabFolder = new TabFolder(parent, SWT.NONE);
		
		TabItem tbtmConfig = new TabItem(tabFolder, SWT.NONE);
		tbtmConfig.setText("Config");
		
		//Composite composite = new Composite(tabFolder, SWT.NONE);
		composite = new Composite(tabFolder, SWT.NONE);
		composite_1 = new Composite(tabFolder, SWT.NONE);
		composite3 = new Composite(tabFolder, SWT.MULTI);
		TabItem tabItem3 = new TabItem(tabFolder, SWT.NONE);
		tabItem3.setText("Fixes");
		tabItem3.setControl(composite3);


		tbtmConfig.setControl(composite);
		
		//Add Groups, Label, textBox, Button
		
		Group groupJdk = new Group(composite, SWT.NONE);
		groupJdk.setText("Java JDK Setting");
		groupJdk.setBounds(10, 10, 766, 52);
		
		Label lblJdk = new Label(groupJdk, SWT.NONE);
		lblJdk.setBounds(10, 22, 90, 15);
		lblJdk.setText("Java JDK Path");
		
		textJdk = new Text(groupJdk, SWT.BORDER);
		textJdk.setBounds(106, 19, 569, 21);
		
		Button btnOpenJdk = new Button(groupJdk, SWT.NONE);
		btnOpenJdk.setBounds(681, 17, 75, 25);
		btnOpenJdk.setText("Open ...");
		
		Group groupLog = new Group(composite, SWT.NONE);
		groupLog.setText("Log Settings");
		groupLog.setBounds(10, 81, 766, 82);
		
		Label lblLogLevel = new Label(groupLog, SWT.NONE);
		lblLogLevel.setBounds(10, 52, 55, 15);
		lblLogLevel.setText("Log Level");
		
		Combo cbbLogLevel = new Combo(groupLog, SWT.NONE);
		cbbLogLevel.setBounds(105, 49, 102, 23);
		cbbLogLevel.setItems("DEBUG","TRACE");
		
		Label lblLogFile = new Label(groupLog, SWT.NONE);
		lblLogFile.setBounds(10, 24, 75, 15);
		lblLogFile.setText("Log File Path");
		
		textLogFilePath = new Text(groupLog, SWT.BORDER);
		textLogFilePath.setBounds(105, 21, 571, 21);
		
		Button btnLogFilePath = new Button(groupLog, SWT.NONE);
		btnLogFilePath.setText("Open ...");
		btnLogFilePath.setBounds(681, 19, 75, 25);
		
		Group groupPj = new Group(composite, SWT.NONE);
		groupPj.setText("Project Settings");
		groupPj.setBounds(10, 174, 766, 122);
		
		textPjRootPath = new Text(groupPj, SWT.BORDER);
		textPjRootPath.setBounds(135, 25, 540, 21);
		
		Button btnPjRootPath = new Button(groupPj, SWT.NONE);
		btnPjRootPath.setText("Open ...");
		btnPjRootPath.setBounds(681, 23, 75, 25);
		
		Label lblProjectRootPath = new Label(groupPj, SWT.NONE);
		lblProjectRootPath.setText("Project Root Path");
		lblProjectRootPath.setBounds(10, 28, 94, 33);
		
		textPjSourcePath = new Text(groupPj, SWT.BORDER);
		textPjSourcePath.setBounds(135, 54, 540, 21);
		
		Button btnPjSourcePath = new Button(groupPj, SWT.NONE);
		btnPjSourcePath.setText("Open ...");
		btnPjSourcePath.setBounds(681, 52, 75, 25);
		
		Label labelPjSourcePath = new Label(groupPj, SWT.NONE);
		labelPjSourcePath.setText("Project Source Path");
		labelPjSourcePath.setBounds(10, 57, 107, 33);
		
		textPjOutputPath = new Text(groupPj, SWT.BORDER);
		textPjOutputPath.setBounds(135, 83, 540, 21);
		
		Button btnPjOutputPath = new Button(groupPj, SWT.NONE);
		btnPjOutputPath.setText("Open ...");
		btnPjOutputPath.setBounds(681, 81, 75, 25);
		
		Label lblPjOutputPath = new Label(groupPj, SWT.NONE);
		lblPjOutputPath.setText("Project Output Path");
		lblPjOutputPath.setBounds(10, 86, 107, 33);
		
		Button btnSave = new Button(composite, SWT.NONE);
		btnSave.setBounds(10, 495, 75, 25);
		btnSave.setText("Save");
		
		Button btnLoad = new Button(composite, SWT.NONE);
		btnLoad.setBounds(91, 495, 75, 25);
		btnLoad.setText("Load");
		
		Button btnRun = new Button(composite, SWT.NONE);
		btnRun.setBounds(684, 495, 75, 25);
		btnRun.setText("Run");
		
		TabItem tbtmFaults = new TabItem(tabFolder, SWT.NONE);
		tbtmFaults.setText("Faults");
		
	//	Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		tbtmFaults.setControl(composite_1);
		
		org.eclipse.swt.widgets.List list = new org.eclipse.swt.widgets.List(composite_1, SWT.BORDER);
		list.setLocation(0, 0);
		list.setSize(394, 531);
		
		//End Add Groups, Label, textBox, Button
		
		
		//Add listeners for FileDialogs
		btnOpenJdk.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				FileDialog fileDialog1 = new FileDialog(parent.getShell());
				fileDialog1.open();
				System.out.println(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
				textJdk.setText(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
			}
		});
		
		btnLogFilePath.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				FileDialog fileDialog1 = new FileDialog(parent.getShell());
				fileDialog1.open();
				System.out.println(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
				textLogFilePath.setText(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
			}
		});
		
		btnPjRootPath.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				FileDialog fileDialog1 = new FileDialog(parent.getShell());
				fileDialog1.open();
				System.out.println(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
				textPjRootPath.setText(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
			}
		});
		
		btnPjSourcePath.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				FileDialog fileDialog1 = new FileDialog(parent.getShell());
				fileDialog1.open();
				System.out.println(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
				textPjSourcePath.setText(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
			}
		});		
		
		btnPjOutputPath.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				FileDialog fileDialog1 = new FileDialog(parent.getShell());
				fileDialog1.open();
				System.out.println(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
				textPjOutputPath.setText(fileDialog1.getFilterPath()+"\\"+fileDialog1.getFileName());
			}
		});	
		//End Add listeners for FileDialogs
		
		//Add listeners for other buttons
		btnOpenJdk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			}
		});
		
		btnLoad.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				loadProperties();
			}
		});
		
		btnSave.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				final Shell shell = new Shell(display);
				switch (e.type) {
				case SWT.Selection:
					if (checkValidSettings()) {
						saveProperites();
						MessageDialog.openInformation(shell, "Info", "Save successfully");
					} else {
						MessageDialog.openError(shell, "Error", "Please ensure to input All the required settings");
					}
					break;
				}
			}
		});
		
		//End Add listeners for other buttons
		
		btnRun.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("btnRunbtnRunbtnRunbtnRunbtnRunbtnRunbtnRunbtnRunbtnRunbtnRunbtnRunbtnRun");
				callFixja();
//				callFixjaOld();
			}
		});		
	}

	//Run hk/polyu/comp/fixja/fixer/Application


private void callFixjaOld() {
	final Shell shell = new Shell(display);
	if (true) {
		//saveProperites();
		// Run java project
		try {
			System.out.println("**********");
			System.out.println("**********");
			// Execute JAID
			runProcess(JDKPath + " -cp " + runtimeEclipseWSPath + "/fixja/lib/*;" + runtimeEclipseWSPath
					+ "/fixja/bin -ea hk/polyu/comp/fixja/fixer/Application --FixjaSettingFile "
					+ runtimeEclipseWSPath + "/fixja/misc/project.properties");
			runJAIDDone = true;
			if (runJaidCount == 0 && fixIdList.isEmpty()) {
				//readPlausibleFixActions(composite3);
				runJaidCount++;
			}
			MessageDialog.openInformation(shell, "Info", "JAID run successfully");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	} else {
		MessageDialog.openError(shell, "Error", "Please ensure to input All the required settings");
	}
}

	private void callFixja() {
		

		RunFixja fixja = new RunFixja(runtimeEclipseWSPath);
		//fixja.run();
	      Thread t = new Thread(fixja);
	    	        t.start();
		
/*		while(true) {
			System.out.println("call fixRetriver");
		

			fixRetriver();
			try {
				TimeUnit.SECONDS.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			}*/
		//return runJAIDDone;

	};
	
	
	
	private void initListeners() {
		
		
		
		
		
	}

	public void printLines(String cmd, InputStream ins) throws Exception {
		String line = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(ins));
		while ((line = in.readLine()) != null) {
			System.out.println(cmd + " " + line);
		}
	}

	public void runProcess(String command) throws Exception {
		Process pro = Runtime.getRuntime().exec(command);
		printLines(" stdout:", pro.getInputStream());
		printLines(" stderr:", pro.getErrorStream());
		pro.waitFor();
		System.out.println(command + " exitValue() " + pro.exitValue());
	}

	public void initProperties() {
		Properties prop = new Properties();
		InputStream in = getClass().getResourceAsStream("initProject.properties");
		try {
			prop.load(in);
			projectRootDirText.setText(prop.getProperty("ProjectRootDir"));
			methodToFixText.setText(prop.getProperty("MethodToFix"));
			in.close();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public void saveProperites() {
		try {
			Properties properties = new Properties();
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			IWorkspaceRoot root = workspace.getRoot();
			properties.setProperty("JDKDir", JDKFormText.getText());

			Properties properties2 = new Properties();
			properties2.setProperty("LogFile", logFileText.getText());
			properties2.setProperty("LogLevel", logLevelText.getText());

			Properties properties3 = new Properties();
			properties3.setProperty("ProjectRootDir", projectRootDirText.getText());
			properties3.setProperty("ProjectSourceDir", projectSourceDirText.getText());
			properties3.setProperty("ProjectOutputDir", projectOutputDirText.getText());

			Properties properties4 = new Properties();
			properties4.setProperty("ProjectLib", projectLibText.getText());

			Properties properties5 = new Properties();
			properties5.setProperty("ProjectTestSourceDir", projectTestSourceDirText.getText());
			properties5.setProperty("ProjectTestOutputDir", projectTestOutputDirText.getText());

			Properties properties6 = new Properties();
			properties6.setProperty("ProjectTestsToInclude", projectTestsToIncludeText.getText());

			Properties properties7 = new Properties();
			properties7.setProperty("ProjectTestsToExclude", projectTestsToExcludeText.getText());
			properties7.setProperty("ProjectExtraClasspath", projectExtraClasspathText.getText());
			properties7.setProperty("ProjectCompilationCommand", projectCompilationCommandText.getText());
			properties7.setProperty("ProjectExecutionCommand", projectExecutionCommandText.getText());
			properties7.setProperty("MethodToFix", methodToFixText.getText());

			System.out.println("Testing:" + Paths.get("").toAbsolutePath().toString());
			File file = new File(runtimeEclipseWSPath + "/fixja/misc/project.properties");
			FileOutputStream fileOut = new FileOutputStream(file);
			properties.store(fileOut, header1String);
			properties2.store(fileOut, header2String);
			properties3.store(fileOut, header3String);
			properties4.store(fileOut, header4String);
			properties5.store(fileOut, header5String);
			properties6.store(fileOut, header6String);
			properties7.store(fileOut, header7String);
			fileOut.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void loadProperties() {
		final Shell shell = new Shell(display);
		final Display display = Display.getDefault();
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				final Shell shell = new Shell(display);
				shell.setText("Valid Fixes");
				FileDialog dlg = new FileDialog(shell, SWT.MULTI);
				if (dlg.open() != null) {
					String[] names = dlg.getFileNames();
					System.out.println("File location: " + dlg.getFilterPath());
					for (String name : names) {
						String filePath = dlg.getFilterPath() + "\\" + name;
						System.out.println("FilePath= " + filePath);

						// Load properties
						Properties prop = new Properties();
						File file = new File(filePath);
						InputStream in;
						try {
							in = new FileInputStream(file);
							prop.load(in);
							logFileText.setText(prop.getProperty("LogFile"));
							logLevelText.setText(prop.getProperty("LogLevel"));
							projectRootDirText.setText(prop.getProperty("ProjectRootDir"));
							projectSourceDirText.setText(prop.getProperty("ProjectSourceDir"));
							projectOutputDirText.setText(prop.getProperty("ProjectOutputDir"));
							projectLibText.setText(prop.getProperty("ProjectLib"));
							projectTestSourceDirText.setText(prop.getProperty("ProjectTestSourceDir"));
							projectTestOutputDirText.setText(prop.getProperty("ProjectTestOutputDir"));
							projectTestsToIncludeText.setText(prop.getProperty("ProjectTestsToInclude"));
							projectTestsToExcludeText.setText(prop.getProperty("ProjectTestsToExclude"));
							projectExtraClasspathText.setText(prop.getProperty("ProjectExtraClasspath"));
							projectCompilationCommandText.setText(prop.getProperty("ProjectCompilationCommand"));
							projectExecutionCommandText.setText(prop.getProperty("ProjectExecutionCommand"));
							methodToFixText.setText(prop.getProperty("MethodToFix"));
							in.close();
							MessageDialog.openInformation(shell, "Info", "Load successfully");
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}
			}
		});
	}

	public void oldReadPlausibleFixActions(Composite Xcomposite) {		
		final Display display = Display.getDefault();
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

//				final Shell shell = new Shell(display);
//				shell.setText("Valid Fixes");

				// Start Read content from plausible_log.txt
				StringBuilder sb = new StringBuilder();
				BufferedReader br = null;						
				
				if(runJAIDDone){
					try {
						br = new BufferedReader(new FileReader(testCaseProjectPath+"/fixja_output/plausible_fix_actions.log"));
						//Demo Testing to bypass the long execution waiting of JAID
						//br = new BufferedReader(new FileReader("C:/Users/Kaming/Desktop/plausible_fix_actions.log"));
						String line;
						StringBuilder fixCode = new StringBuilder();
						FixObject validFix = new FixObject();
						while ((line = br.readLine()) != null) {
							if (sb.length() > 0) {
								sb.append("\n");
							}
							sb.append(line);
							if(line.contains("[main ] INFO  - FixAction")){
								String fixID = line.substring(line.indexOf("fixId=")+6,line.indexOf(", seed"));
								fixIdList.add(fixID);
								//Object
								validFix.setFixID(Integer.parseInt(fixID));
								String seed = line.substring(line.indexOf("seed=")+5,line.indexOf(", simi"));
								seedList.add(seed);
								//Object
								validFix.setSeed(seed);
								String sim = line.substring(line.indexOf("simi=")+5,line.indexOf(", score"));
								simList.add(sim);
								//Object
								validFix.setSim(sim);
								String score = line.substring(line.indexOf("score=")+6,line.indexOf(", fix=["));
								scoreList.add(score);
								//Object
								validFix.setScore(score);
								String fixCodeOpen = line.substring(line.indexOf("fix=[")+5);
								fixCode.append(fixCodeOpen);
							} else {
								if (!line.equals("]") && !line.equals("}]") && !line.contains("[main ] INFO") && !line.contains(", location=")){
									fixCode.append(line);
									fixCode.append("\n");
								} else {
									if(line.equals("]") || line.equals("}]")){
										String printFixCode = fixCode.toString();
										fixCodeList.add(printFixCode);
										validFix.setFixCode(printFixCode);
										fixCode = new StringBuilder();
									}
									if(line.contains(", location=")){
										String location = line.substring(line.indexOf("location")+9);
										locationList.add(location);
										validFix.setLocation(location);
										//Add to ObjectList
										FixObjectList.add(validFix);
										validFix = new FixObject();
									}
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						try {
							if (br != null) {
								br.close();
							}
						} catch (IOException ex) {
							ex.printStackTrace();
						}
					}
					// All content in plausible_fix_actions.txt
					String contents = sb.toString();
				    //System.out.println(contents);
	//						Pattern regex = Pattern.compile("(\\{[^\\{\\}]*?\\})");
	//						Matcher regexMatcher = regex.matcher(contents);
	//						while (regexMatcher.find()) {
	//						    // matched text: regexMatcher.group()
	//						    System.out.println(regexMatcher.group());
	//						}
					System.out.println("fixIdList Size: " +fixIdList.size());
					System.out.println("seedList Size: " +seedList.size());
					System.out.println("simList Size: " +simList.size());
					System.out.println("scoreList Size: " +scoreList.size());
					System.out.println("fixCodeList Size: " +fixCodeList.size());
					System.out.println("locationList Size: " +locationList.size());
					System.out.println("Valid Fix action: "+FixObjectList.size());
					// End Read content from plausible_fix_actions.txt
					///////////////////////End prepare validFix List//////////////////////////////////////		
					
					// Start draw the fixes UI
					
					GridLayout gridLayout = new GridLayout();
					gridLayout.numColumns = 2;
					
					Xcomposite.setLayout(gridLayout);
				
					final Tree tree = new Tree(Xcomposite, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
					treeField.widthHint = 400;
					treeField.heightHint = 200;
					tree.setLayoutData(treeField);
				
				
					for (int loopIndex0 = 1; loopIndex0 <= FixObjectList.size(); loopIndex0++) {
						TreeItem treeItem0 = new TreeItem(tree, 0);
						treeItem0.setText("Fix " + loopIndex0);
						TreeItem treeItem1 = new TreeItem(treeItem0, 0);
						
						treeItem1.setText("ID");
						TreeItem treeItem1Leaf1 = new TreeItem(treeItem1, 0);
						treeItem1Leaf1.setText(fixIdList.get(loopIndex0 - 1));
						
						TreeItem treeItem2 = new TreeItem(treeItem0, 0);
						treeItem2.setText("Fix/Action");
						TreeItem treeItem2Leaf1 = new TreeItem(treeItem2, 0);
						treeItem2Leaf1.setText(fixCodeList.get(loopIndex0 - 1));
						
						TreeItem treeItem3 = new TreeItem(treeItem0, 0);
						treeItem3.setText("Strategy");
						TreeItem treeItem3Leaf1 = new TreeItem(treeItem3, 0);
						treeItem3Leaf1.setText(seedList.get(loopIndex0 - 1));
						
						TreeItem treeItem4 = new TreeItem(treeItem0, 0);
						treeItem4.setText("Similarity");
						TreeItem treeItem4Leaf1 = new TreeItem(treeItem4, 0);
						treeItem4Leaf1.setText(simList.get(loopIndex0 - 1));
						
						TreeItem treeItem5 = new TreeItem(treeItem0, 0);
						treeItem5.setText("Score");
						TreeItem treeItem5Leaf1 = new TreeItem(treeItem5, 0);
						treeItem5Leaf1.setText(scoreList.get(loopIndex0 - 1));
					}
					
					final Text text = new Text(Xcomposite, SWT.BORDER| SWT.V_SCROLL | SWT.H_SCROLL);
					textField.widthHint = 400;
					textField.heightHint = 200;
					text.setLayoutData(textField);
				
					tree.addListener(SWT.Selection, new Listener() {
						@Override
						public void handleEvent(Event event) {
					        String string = "";
					        TreeItem[] selection = tree.getSelection();
					        for (int i = 0; i < selection.length; i++)
					          string += selection[i].getText() + " ";
							if (event.detail == SWT.CHECK) {
								text.setText(string + " was checked.");
							} else {
								text.setText(string + " was selected");
								selectedFixCode = string;
							}
						}
					});
					
					// End draw the fixes UI
				
				
					//							Narrow down the tree fix window				//
					
	//				GridLayout gridLayout = new GridLayout();
	//				gridLayout.numColumns = 2;
	//				
	//				Xcomposite.setLayout(gridLayout);
			 
			
					Button btnFix = new Button(Xcomposite, SWT.PUSH);
					btnFix.setText("Preview Changes");
					btnFix.addListener(SWT.Selection, new Listener() {
					    @Override
						public void handleEvent(Event e) {
								CompareEditorInput cei = new CompareEditorInput(new CompareConfiguration()) {
				
								    @Override
								    protected Object prepareInput(IProgressMonitor arg0)
								            throws InvocationTargetException, InterruptedException {
										CompareItem ancestor = new CompareItem("Common", "contents");
										for(String location :locationList){
											beforeFixCode = location.substring(location.lastIndexOf(":") + 1);
										}
										CompareItem left = new CompareItem("Before Fix", beforeFixCode);
										CompareItem right = new CompareItem("After Fix", selectedFixCode);
										//Demo Testing to show the lines of insertion
										//CompareItem left = new CompareItem("Before Fix", "int to_be_copied = 0, counter = 0; \nMyList result = new MyList(storage.size()); \nto_be_copied = Math.min(n, this.count() - index);\nwhile (counter < to_be_copied) { \n result.extend(this.item()); \n forth(); \n counter++; \n}");
										//CompareItem right = new CompareItem("After Fix", "int to_be_copied = 0, counter = 0; \nMyList result = new MyList(storage.size()); \nto_be_copied = Math.min(n, this.count() - index); \n" + selectedFixCode + "\nwhile (counter < to_be_copied) { \n result.extend(this.item()); \n forth(); \n counter++; \n}");
				
								        getCompareConfiguration().setLeftLabel(left.getName());
								        getCompareConfiguration().setRightLabel(right.getName());
								        getCompareConfiguration().setAncestorLabel(ancestor.getName());
				
								        return new DiffNode(null,Differencer.CONFLICTING,ancestor,left,right);
								    }
								};
					    	  CompareUI.openCompareEditor(cei);
					    }
				    });
					Button applyFixbtn = new Button(Xcomposite, SWT.PUSH);
					applyFixbtn.setText("Apply Fix");
				}
				//						End Narrow down the tree fix window				//				
						
		
//				shell.close(); // if shell.open(), error occur when re-run eclipse plugin application
//				while (!shell.isDisposed()) {
//					if (!display.readAndDispatch()) {
//						display.sleep();
//					}
//				}
			}
		});		
	}

	public static void readPlausibleFixActions(Composite Xcomposite) {
		
		System.out.println("1904021435 readPlausibleFixActionsreadPlausibleFixActionsreadPlausibleFixActions");
		final Display display = Display.getDefault();
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

//				final Shell shell = new Shell(display);
//				shell.setText("Valid Fixes");

				// Start Read content from plausible_log.txt
				StringBuilder sb = new StringBuilder();
//					 br = null;

//				if (runJAIDDone) {
					try {
/*						br = new BufferedReader(
								new FileReader(testCaseProjectPath + "/fixja_output/plausible_fix_actions.log"));*/
						// Demo Testing to bypass the long execution waiting of JAID
						// br = new BufferedReader(new
						// FileReader("C:/Users/Kaming/Desktop/plausible_fix_actions.log"));
						String line;
						StringBuilder fixCode = new StringBuilder();
						FixObject validFix = new FixObject();
						int testCount =0;
						while ((line = br.readLine()) != null && testCount <1000) {
							testCount += 1;
							if (sb.length() > 0) {
								sb.append("\n");							
								}
							sb.append(line);
							if (line.contains("FixAction")) {
								System.out.println("190402 line.contains(\"FixAction\")="+line);
								String fixID = line.substring(line.indexOf("fixId=") + 6, line.indexOf(", seed"));
								fixIdList.add(fixID);
								// Object
								validFix.setFixID(Integer.parseInt(fixID));
								String seed = line.substring(line.indexOf("seed=") + 5, line.indexOf(", simi"));
								seedList.add(seed);
								// Object
								validFix.setSeed(seed);
								String sim = line.substring(line.indexOf("simi=") + 5, line.indexOf(", score"));
								simList.add(sim);
								// Object
								validFix.setSim(sim);
								String score = line.substring(line.indexOf("score=") + 6, line.indexOf(", fix=["));
								scoreList.add(score);
								// Object
								validFix.setScore(score);
								String fixCodeOpen = line.substring(line.indexOf("fix=[") + 5);
								fixCode.append(fixCodeOpen);
							} else {
								System.out.println("190402 else contain="+line);
								if (!line.equals("]") && !line.equals("}]") && !line.contains("[main ] INFO")
										&& !line.contains(", location=")) {
									System.out.println("190402 else contain2");
									fixCode.append(line);
									fixCode.append("\n");
								} else {
									if (line.equals("]") || line.equals("}]")) {
										System.out.println("190402 else contain3="+line);
										String printFixCode = fixCode.toString();
										fixCodeList.add(printFixCode);
										validFix.setFixCode(printFixCode);
										fixCode = new StringBuilder();
									}
									if (line.contains(", location=")) {
										System.out.println("190402 else location="+line);
										String location = line.substring(line.indexOf("location") + 9);
										locationList.add(location);
										validFix.setLocation(location);
										// Add to ObjectList
										FixObjectList.add(validFix);
										validFix = new FixObject();
									}
								}
							}
						}
					} /*catch (IOException e) {
						e.printStackTrace();
					} */ catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}finally {
				/*		try {
							if (br != null) {
								br.close();
							}
						} catch (IOException ex) {
							
						}*/
					}
					// All content in plausible_fix_actions.txt
					String contents = sb.toString();
					// System.out.println(contents);
					// Pattern regex = Pattern.compile("(\\{[^\\{\\}]*?\\})");
					// Matcher regexMatcher = regex.matcher(contents);
					// while (regexMatcher.find()) {
					// // matched text: regexMatcher.group()
					// System.out.println(regexMatcher.group());
					// }
					System.out.println("fixIdList Size: " + fixIdList.size());
					System.out.println("seedList Size: " + seedList.size());
					System.out.println("simList Size: " + simList.size());
					System.out.println("scoreList Size: " + scoreList.size());
					System.out.println("fixCodeList Size: " + fixCodeList.size());
					System.out.println("locationList Size: " + locationList.size());
					System.out.println("Valid Fix action: " + FixObjectList.size());
					// End Read content from plausible_fix_actions.txt
					/////////////////////// End prepare validFix
					// List//////////////////////////////////////

					// Start draw the fixes UI
					
					

					GridLayout gridLayout = new GridLayout();

					Xcomposite.setLayout(gridLayout);

					final Tree tree = new Tree(Xcomposite, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
					treeField.widthHint = 400;
					treeField.heightHint = 200;
					tree.setLayoutData(treeField);

					for (int loopIndex0 = 1; loopIndex0 <= FixObjectList.size(); loopIndex0++) {
						System.out.println("loopIndex0 =" + loopIndex0);

						TreeItem treeItem0 = new TreeItem(tree, 0);
						treeItem0.setText("Fix " + loopIndex0);
						TreeItem treeItem1 = new TreeItem(treeItem0, 0);

						treeItem1.setText("ID");
						TreeItem treeItem1Leaf1 = new TreeItem(treeItem1, 0);
						treeItem1Leaf1.setText(fixIdList.get(loopIndex0 - 1));

						TreeItem treeItem2 = new TreeItem(treeItem0, 0);
						treeItem2.setText("Fix/Action");
						TreeItem treeItem2Leaf1 = new TreeItem(treeItem2, 0);
						treeItem2Leaf1.setText(fixCodeList.get(loopIndex0 - 1));

						TreeItem treeItem3 = new TreeItem(treeItem0, 0);
						treeItem3.setText("Strategy");
						TreeItem treeItem3Leaf1 = new TreeItem(treeItem3, 0);
						treeItem3Leaf1.setText(seedList.get(loopIndex0 - 1));

						TreeItem treeItem4 = new TreeItem(treeItem0, 0);
						treeItem4.setText("Similarity");
						TreeItem treeItem4Leaf1 = new TreeItem(treeItem4, 0);
						treeItem4Leaf1.setText(simList.get(loopIndex0 - 1));

						TreeItem treeItem5 = new TreeItem(treeItem0, 0);
						treeItem5.setText("Score");
						TreeItem treeItem5Leaf1 = new TreeItem(treeItem5, 0);
						treeItem5Leaf1.setText(scoreList.get(loopIndex0 - 1));
					}

					final Text text = new Text(Xcomposite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

					text.setLayoutData(new GridData(400, 200));

					tree.addListener(SWT.Selection, new Listener() {
						@Override
						public void handleEvent(Event event) {
							String string = "";
							TreeItem[] selection = tree.getSelection();
							for (int i = 0; i < selection.length; i++)
								string += selection[i].getText() + " ";
							if (event.detail == SWT.CHECK) {
								text.setText(string + " was checked.");
							} else {
								text.setText(string + " was selected");
								selectedFixCode = string;
							}
						}
					});

					// End draw the fixes UI

					// Narrow down the tree fix window //

					// GridLayout gridLayout = new GridLayout();
					// gridLayout.numColumns = 2;
					//
					// Xcomposite.setLayout(gridLayout);

					Button btnFix = new Button(Xcomposite, SWT.PUSH);
					btnFix.setText("Preview Changes");
					btnFix.addListener(SWT.Selection, new Listener() {
						@Override
						public void handleEvent(Event e) {
							CompareEditorInput cei = new CompareEditorInput(new CompareConfiguration()) {

								@Override
								protected Object prepareInput(IProgressMonitor arg0)
										throws InvocationTargetException, InterruptedException {
									CompareItem ancestor = new CompareItem("Common", "contents");
									for (String location : locationList) {
										beforeFixCode = location.substring(location.lastIndexOf(":") + 1);
									}
									CompareItem left = new CompareItem("Before Fix", beforeFixCode);
									CompareItem right = new CompareItem("After Fix", selectedFixCode);
									// Demo Testing to show the lines of insertion
									// CompareItem left = new CompareItem("Before Fix", "int to_be_copied = 0,
									// counter = 0; \nMyList result = new MyList(storage.size()); \nto_be_copied =
									// Math.min(n, this.count() - index);\nwhile (counter < to_be_copied) { \n
									// result.extend(this.item()); \n forth(); \n counter++; \n}");
									// CompareItem right = new CompareItem("After Fix", "int to_be_copied = 0,
									// counter = 0; \nMyList result = new MyList(storage.size()); \nto_be_copied =
									// Math.min(n, this.count() - index); \n" + selectedFixCode + "\nwhile (counter
									// < to_be_copied) { \n result.extend(this.item()); \n forth(); \n counter++;
									// \n}");

									getCompareConfiguration().setLeftLabel(left.getName());
									getCompareConfiguration().setRightLabel(right.getName());
									getCompareConfiguration().setAncestorLabel(ancestor.getName());

									return new DiffNode(null, Differencer.CONFLICTING, ancestor, left, right);
								}
							};
							CompareUI.openCompareEditor(cei);
						}
					});
					Button applyFixbtn = new Button(Xcomposite, SWT.PUSH);
					applyFixbtn.setText("Apply Fix");
//				}
				// End Narrow down the tree fix window //
					System.out.println("End drawing fix tab!!!!!!!!!!!!");

//				shell.close(); // if shell.open(), error occur when re-run eclipse plugin application
//				while (!shell.isDisposed()) {
//					if (!display.readAndDispatch()) {
//						display.sleep();
//					}
//				}
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
	}

	@Override
	public void saveState(IMemento memento) {
		if (logFileText != null)
			memento.putString("logFile", logFileText.getText());
		if (logLevelText != null)
			memento.putString("logLevel", logLevelText.getText());
		if (projectRootDirText != null)
			memento.putString("projectRootDir", projectRootDirText.getText());
		if (projectSourceDirText != null)
			memento.putString("projectSourceDir", projectSourceDirText.getText());
		if (projectOutputDirText != null)
			memento.putString("projectOutputDir", projectOutputDirText.getText());
		if (projectLibText != null)
			memento.putString("projectLib", projectLibText.getText());
		if (projectTestSourceDirText != null)
			memento.putString("projectTestSourceDir", projectTestSourceDirText.getText());
		if (projectTestOutputDirText != null)
			memento.putString("projectTestOutputDir", projectTestOutputDirText.getText());
		if (projectTestsToIncludeText != null)
			memento.putString("projectTestsToInclude", projectTestsToIncludeText.getText());
		if (projectTestsToExcludeText != null)
			memento.putString("projectTestsToExclude", projectTestsToExcludeText.getText());
		if (projectExtraClasspathText != null)
			memento.putString("projectExtraClasspath", projectExtraClasspathText.getText());
		if (projectCompilationCommandText != null)
			memento.putString("projectCompilationCommand", projectCompilationCommandText.getText());
		if (projectExecutionCommandText != null)
			memento.putString("projectExecutionCommand", projectExecutionCommandText.getText());
		if (methodToFixText != null)
			memento.putString("methodToFix", methodToFixText.getText());
		System.out.println("saveState-> memento");

	}

	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		System.out.println("Intialize the view");
		this.memento = memento;
	}

	public boolean checkValidSettings() {
		if (JDKFormText.getText().isEmpty() || logFileText.getText().isEmpty() || logLevelText.getText().isEmpty()
				|| projectRootDirText.getText().isEmpty() || projectSourceDirText.getText().isEmpty()
				|| projectOutputDirText.getText().isEmpty() || projectLibText.getText().isEmpty()
				|| projectTestSourceDirText.getText().isEmpty() || methodToFixText.getText().isEmpty()) {
			return false;
		}
		return true;
	}

	// Try to make a console at launched plugin
	private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}
	
	
	
	public void fixRetriver() {
		
	   Map<LineLocation, Set<FixAction>> fixActionMap;

	   fixActionMap = hk.polyu.comp.fixja.fixer.log.LoggingService.pullFixActionMap();
		
		System.out.println("fixRetriverfixRetriverfixRetriverfixRetriver:" +fixActionMap);
		
		
	}
	
	
	public static void receiverTest() {
		
		
		System.out.println("20190320 over over");
	}
	
public  static void receiverFixActions(Map<LineLocation, Set<FixAction>> fixActionMap, int size) {
		
	countReceiver += 1;
		System.out.println("2019402 fixActions"+size);
		
		if(countReceiver>1000) return;
		 
//           addFileLogger(FixerOutput.LogFile.ALL_FIX_ACTION, Level.DEBUG);
         //  FixerOutput.LogFile fixActionLog = FixerOutput.LogFile.ALL_FIX_ACTION;

        //   FileUtil.ensureEmptyFile(fixActionLog.getLogFilePath());
           //PrintWriter writer = new PrintWriter(fixActionLog.getLogFilePath().toString(), StandardCharsets.UTF_8.toString())){
               for (Map.Entry<LineLocation, Set<FixAction>> fixActionEntry : fixActionMap.entrySet()) {
                   System.out.println(fixActionEntry.getKey().toString() + "; Fix size:: " + fixActionEntry.getValue().size() + "\n");
               }
               System.out.println("==================================190321\n");
               for (Map.Entry<LineLocation, Set<FixAction>> fixActionEntry : fixActionMap.entrySet()) {
                   for (FixAction fixAction : fixActionEntry.getValue()) {
                       System.out.println(fixAction.toString());
                       
                       Reader inputString = new StringReader(fixAction.toString());

                       br = new BufferedReader(inputString);
                       
                       readPlausibleFixActions(composite3);
                   }
               }
               System.out.println("Generated fixactions190321:" + size);

  
       
	}
}